/*
 *  File: random.c - Gaim RVP plug-in implementation.
 *  Copyright (C) 2003  Weihua Sun (weihua@lucent.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "random.h"

/*
 * The following source code is from the Text book of "Data Structures
 * and Program Design in C++", Robert Kruse, Alex Ryba, Alexander J. Ryba,
 * 1998 Prentice Hall.
 */


#define max_int INT_MAX		/* 2147483647 maximum (signed) int value */

int seed, multiplier, add_on;

int reseed()
{
	seed = seed * multiplier + add_on;
	return seed;
}


void init_seed(int pseudo)
{
	if (pseudo)
		seed = 1;
	else
		seed = time(NULL) % max_int;
	multiplier = 2743;
	add_on = 5923;
}


double random_real()
{
	double max = max_int + 1.0;
	double temp = reseed();
	if (temp < 0)
		temp = temp + max;
	return temp/max;
}


int random_integer(int low, int high)
{
	if (low > high)
		return random_integer(high, low);
	else
		return ((int)((high - low + 1) * random_real())) + low;
}


#ifdef TESTRAND

int main()
{
	int i, num;
	int st = 1;

	init_seed(st);

	for(i=0; i<16; i++) {
		num = random_integer(0, 15);
		printf("%d. %d\n", i, num);
	}
	return 0;
}

#endif

