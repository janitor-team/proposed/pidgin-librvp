/*
 * header for Digest code
 */
#ifndef _RVP_DIGEST_H
#define _RVP_DIGEST_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Gaim includes */
#include <version.h>
#ifdef PURPLE_MAJOR_VERSION
#include "pidgin.h"
#include "gaim-compat.h"
#endif
#include <connection.h>

#if GAIM_MAJOR_VERSION < 2 /* _HAVE_MD5_H */
/*
 * Gaim 1.5 comes with a separate MD5 header file
 */
#include <md5.h>
#undef GCRYPT_MD5 /* because it'll break */
#else /* don't _HAVE_MD5_H */

#ifdef GCRYPT_MD5
#include <gcrypt.h>
#else /* don't have GCRYPT_MD5 */
#define NOMD5 /* for now, that means no MD5 at all */
#endif /* GCRYPT_MD5 */
#endif /* GAIM_MAJOR_VERSION */

#ifndef NOMD5
gchar *get_auth_digest( GaimConnection *, gchar *, gchar *, gchar *, gchar *,
                        gchar *pass );
#endif /* NO MD5 */

#endif
