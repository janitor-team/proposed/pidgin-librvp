-*-Text-*-
librvp release 0.9.7
- started restructuring code to make it principally for Pidgin, with
  Gaim as a compatibility layer.
- MD5 auth implementation for Pidgin
- redo message encoding to operate (as best I can tell) correctly
- minor doc updates

librvp release 0.9.6a
- place Pidgin pixmaps properly

librvp release 0.9.6
- Pidgin (what used be Gaim 2) support. That's about it. See Changelog
  for the gory details.
- --with-gaim is no longer a supported configure option; use
  --with-pkgpath /dir/containing/pidgin.pc to tell configure where to
  look first. It has a preference for Pidgin over Gaim.

librvp release 0.9x, x < .6
- guess I forgot to update the news file...

librvp release 0.8
- fixed two subscription-related bugs: one that would randomly cause
  your self-subscription ID to get lost, resulting in you being
  offline without knowing it, and one that caused unsubscribes to mess
  everything up resulting in you getting kicked offline.
- group chat should now be working about as well as I'd hope. You can
  start a group chat yourself by choosing "Add Chat" from the main
  Gaim menu, thus creating a blank chat in your buddy list;
  double-clicking this brings up an empty chat which you can invite
  people to.
- the My Hostname option is now obsolete; the plugin determines what to
  use for a callback address when it connects to the server. I probably
  need to clean out the option and maybe respect the main Gaim option
  that controls this.

librvp release 0.7
- finally I am putting updates in the NEWS file to keep you from
  having to read the changelog.
- this release features pretty much complete support for blocking and
  unblocking users.
- there is a new prefs option, "disable assertion credentials". When set,
  Gaim will never send an assertion credential, which you can find out
  more about in the RVP specs; in short, it means that buddies have to
  successfully authenticate with the server through a
  username/password before the server will pass messages from them to
  you. The alternative is that they simply tell the server who they
  are and the server believes that.

  SHOULD YOU SET THIS OPTION?

  If possible, it's probably a good idea, particularly if you've run
  either previous releases of librvp, or the original gaim+rvp code
  that it's based on as these used assertion ACLs unconditionally. The
  new code will respect the existing server's saved credentials unless
  you set this option. HOWEVER, IF YOUR SERVER DOES NOT REQUIRE YOU TO
  AUTHENTICATE THERE'S A GOOD CHANCE YOU SHOULDN'T TOUCH THIS SETTING,
  as it'll probably break your messaging capability completely.
- in a debug build, you now have the option to tweak the subscribe
  timeout, which should help in tracking down bugs that only occur on
  resubscribe.
- this release will build and run with Gaim 2.0beta2, but is currently
  missing some features such as the ability to send messages.
- there's now an Account Action to import the buddy list from the old
  version of this plugin (gaim+rvp) and also from an export from
  Microsoft Messenger (.ctt file)


