#!/usr/bin/perl -w
#
# import MSN settings from a registry file
#
# extract the data using regedit's "export to Windows 95/NT4 registry
# file" option, so you end up with a readable text file.
#
# Usage:
# use regedit to dump out the
# HKCU\Software\Microsoft\Exchange\Messenger as a REGEDIT4 file. Then
# import_msn_settings.pl registryfile.reg
use strict;
use XML::Simple;
use Data::Dumper;

die "usage: $0 registryfile.reg\n" unless $#ARGV == 0;

my $groupname = "Microsoft Exchange Instant Messaging";

my $accounts = XMLin( $ENV{HOME} . "/.gaim/accounts.xml",
                      ForceArray => 1, KeyAttr => [] );
my $blist = XMLin( $ENV{HOME} . "/.gaim/blist.xml",
                   ForceArray => 1, KeyAttr => [] );


# connection methods:
# 0 = default, 1 = polling, 2 = fixed ports

my $ok = 0;
my $account;
my %accounts;
my %profile;
while (<>) {
    s/\r//;
    if ( !$ok ) {
        die "this doesn't look like a registry file\n($_)" unless /^REGEDIT4$/;
        $ok = 1;
    }

    next if /^$/;

    # registry folder
    if ( m{^\[HKEY_CURRENT_USER\\Software\\Microsoft\\Exchange\\Messenger(.*)\]} ) { # }}
        my $tag = $1;
        if ( $tag =~ /Contacts$/ ) {
            $account = $tag;
            $account =~ s/.Profiles.(.*).Contacts/$1/;
            $account =~ s{^http://(.*?)/.*/([^/]+)$}{$2\@$1}; #}{};

            my @bits;
            $accounts{$account} = \@bits;
        } elsif ( $tag eq '\Profiles' ) {
            $profile{connection} = 0;
        }
    } elsif ( defined( $account ) and defined ( $accounts{$account} )) {
        my ( $user ) = m{"([^"]+)"}; #};
        push @{$accounts{$account}}, $user;
    } elsif ( defined( $profile{connection})) {
        my ( $key, $value ) = split( /=/, $_, 2 );
        $profile{connection} = $value if $key eq "Connection Method";
        $profile{poll} = $value if $key eq "Polling Period";
        $profile{ports} = $value if $key eq "Ports";
    }
}

die "Nothing to do!" unless %accounts;

for my $acc ( keys %accounts ) {
    my $thisacc;
    for my $a ( @{$accounts->{account}}) {
        if ( $a->{name} eq $acc ) {
            $thisacc = $a;
            last;
        }
    }

    if ( !defined( $thisacc )) {
        # need to kludge it up.
        my %newacc = (
                      protocol => [ 'prpl-rvp' ],
                      name => [ $acc ],
                      settings => [
                                  ],
                     );
        push @{$accounts->{account}}, \%newacc;
        $thisacc = $accounts->{account}->[-1];
    }

    if ( $profile{connection} eq "dword:00000000" ) {
        # default connection type
    } elsif ( $profile{connection} eq "dword:00000001" ) {
        # polling, not supported yet
        print STDERR "Polling connection not supported yet\n";
    } elsif ( $profile{connection} eq "dword:00000002" ) {
        if ( !defined( $profile{ports})) {
            print STDERR "Fixed port connection with no ports!\n";
        } else {
            my %setting;
            $profile{ports} =~ s/"//g;
            my ( $low, $high ) = split( '-', $profile{ports}, 2 );

            # xxx port range unsupported
            $setting{name} = 'port';
            $setting{type} = 'int';
            $setting{value} = $low;
            # xxx
            push @{$thisacc->{settings}}, \%setting;
        }
    }
}

open( ACC, ">accounts.xml.merged" ) or die "Can't create new accounts file: $!";
print ACC XMLout( $accounts, XMLDecl => 1, RootName => 'accounts', KeyAttr => [], NoSort => 1 );
close( ACC );

# find the correct group
my $group;
for my $g ( @{$blist->{blist}->[0]->{group}}) {
    if ( $g->{name} eq $groupname ) {
        $group = $g;
        last;
    }
}

# xxx
die "no group" unless $group;

# loop through each account & buddy, and add them to the structure
for my $acc ( keys %accounts ) {
    for my $buddy ( @{$accounts{$acc}}) {
        my %contact;
        $contact{buddy} = [
                           {
                            protocol => 'prpl-rvp',
                            account => $acc,
                            name => [ $buddy ]
                           }
                          ];
        push @{$group->{contact}}, \%contact;
    }
}

open( BL, ">blist.xml.merged" ) or die "Can't create new blist file: $!";
print BL XMLout( $blist, XMLDecl => 1, RootName => 'gaim', KeyAttr => [], NoSort => 1 );
close( BL );

print "Generated accounts.xml.merged and blist.xml.merged.\n";
